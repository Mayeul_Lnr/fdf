/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/04 15:05:00 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/11/09 14:25:17 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./fdf.h"

int	list_push_back(t_list **list, void *line)
{
	t_list	*new;
	t_list	*head;

	new = malloc(sizeof(t_list));
	if (!new)
	{
		free(line);
		return (-1);
	}
	new->data = line;
	new->next = NULL;
	if (!*list)
		*list = new;
	else
	{
		head = *list;
		while (head->next)
			head = head->next;
		head->next = new;
	}
	return (0);
}

void	*list_free(t_list **list)
{
	t_list	*temp;

	while (*list)
	{
		temp = *list;
		free(temp->data);
		*list = temp->next;
		free(temp);
	}
	return (NULL);
}
