/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   le_futur.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/20 15:44:34 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/12/22 22:11:45 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./fdf.h"

int	transform(t_fdf *fdf, int x, int y)
{
	t_pcrparam	p;

	p.sin = sin(fdf->c->longitude);
	p.cos = cos(fdf->c->longitude);
	p.x1 = fdf->c->size * (x - (float)(fdf->m->w - 1) / 2);
	p.y1 = fdf->c->size * (y - (float)(fdf->m->h - 1) / 2);
	p.x2 = p.x1 * p.cos - p.y1 * p.sin;
	p.y2 = p.x1 * p.sin + p.y1 * p.cos;
	fdf->m->transform_x[x + y * fdf->m->w] = p.x2 + fdf->d->w / 2
		+ fdf->c->off_x;
	p.cos = -cos(fdf->c->colatitude);
	p.sin = sin(fdf->c->colatitude);
	fdf->m->transform_y[x + y * fdf->m->w] = p.y2 * p.cos + (fdf->d->h / 2)
		- fdf->c->size * fdf->m->map[x + y * fdf->m->w] * p.sin
		* fdf->c->hscale + fdf->c->off_y;
	return (0);
}

static int	point_color(int z, t_fdf *fdf)
{
	return (pcr_fade(((float)z - fdf->m->min) / (fdf->m->max - fdf->m->min),
			CR_MIN, CR_MAX));
}

static int	draw_vertex(int x, int y, t_fdf *fdf)
{
	t_pcrparam	p;

	p.cr1 = point_color(fdf->m->map[x + y * fdf->m->w], fdf);
	p.x1 = fdf->m->transform_x[y * fdf->m->w + x];
	p.y1 = fdf->m->transform_y[y * fdf->m->w + x];
	if (x != fdf->m->w - 1)
	{
		p.x2 = fdf->m->transform_x[y * fdf->m->w + x + 1];
		p.y2 = fdf->m->transform_y[y * fdf->m->w + x + 1];
		p.cr2 = point_color(fdf->m->map[x + 1 + y * fdf->m->w], fdf);
		pcr_line(fdf->d, p);
	}
	if (y != fdf->m->h - 1)
	{
		p.x2 = fdf->m->transform_x[fdf->m->w * (y + 1) + x];
		p.y2 = fdf->m->transform_y[fdf->m->w * (y + 1) + x];
		p.cr2 = point_color(fdf->m->map[x + (y + 1) * fdf->m->w], fdf);
		pcr_line(fdf->d, p);
	}
	return (0);
}

int	draw_transform(t_fdf *fdf)
{
	int		x;
	int		y;
	float	a;

	a = fdf->c->longitude;
	x = -1;
	while (++x < fdf->m->w)
	{
		y = -1;
		while (++y < fdf->m->h)
		{
			draw_vertex((a > PI) * (fdf->m->w - 2 * x - 1) + x,
				(a > PI / 2 && a < 3 * PI / 2) * (fdf->m->h - 2 * y - 1) + y,
				fdf);
		}
	}
	return (0);
}

int	le_futur(t_fdf *fdf)
{
	int	x;
	int	y;

	x = -1;
	while (++x < fdf->m->w)
	{
		y = -1;
		while (++y < fdf->m->h)
			transform(fdf, x, y);
	}
	draw_transform(fdf);
	pcr_display(fdf->d);
	return (0);
}
