/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/03 17:36:22 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/12/22 22:31:05 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./fdf.h"

int	*convert_line(char *line, t_map *map)
{
	char	**split;
	int		current_w;
	int		*ret;
	int		i;

	split = ft_split(line, ' ', &current_w);
	if (!split)
		return (NULL);
	if (map->w < 0)
		map->w = current_w;
	else if (map->w != current_w)
		return (ft_splitfree(split));
	ret = malloc(map->w * sizeof(int));
	if (!ret)
		return (ft_splitfree(split));
	i = -1;
	while (++i < map->w)
	{
		if (ft_atoichk(split[i], ret + i))
		{
			free(ret);
			return (ft_splitfree(split));
		}
	}
	return (ret + (long)ft_splitfree(split));
}

t_list	*read_file(int fd, t_map *map)
{
	t_list	*ret;
	char	*s_line;
	int		*i_line;
	int		n;

	n = 1;
	ret = NULL;
	while (n > 0)
	{
		n = get_next_line(fd, &s_line);
		if (n < 0)
			return (list_free(&ret));
		if (!n)
		{
			free(s_line);
			break ;
		}
		i_line = convert_line(s_line, map);
		free(s_line);
		if (!i_line || list_push_back(&ret, i_line) < 0)
			return (list_free(&ret));
		map->h++;
	}
	return (ret);
}

int	convert_file(t_list *file, t_map *map)
{
	int	i;
	int	j;

	map->map = malloc(map->w * map->h * sizeof(int));
	if (!map->map)
		return (-1);
	map->max = ((int *)file->data)[0];
	map->min = ((int *)file->data)[0];
	i = 0;
	while (file)
	{
		j = 0;
		while (j < map->w)
		{
			map->map[i] = ((int *)file->data)[j];
			if (map->max < map->map[i])
				map->max = map->map[i];
			if (map->min > map->map[i])
				map->min = map->map[i];
			i++;
			j++;
		}
		file = file->next;
	}
	return (0);
}

int	fdf_parsing(char *file, t_map *map)
{
	t_list	*lines;
	int		fd;
	int		ret;

	fd = open(file, O_RDONLY);
	if (fd < 0)
		return (-1);
	map->w = -1;
	map->h = 0;
	lines = read_file(fd, map);
	if (!lines)
		return (-1);
	ret = convert_file(lines, map);
	if (map->min == map->max)
		map->max++;
	list_free(&lines);
	close(fd);
	return (ret);
}
