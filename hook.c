/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/03 14:37:56 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/12/22 22:29:52 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./fdf.h"

static int	wasd_keys(int x, t_fdf *fdf)
{
	if (x == 0x77)
		fdf->c->off_y += 10;
	else if (x == 0x73)
		fdf->c->off_y -= 10;
	else if (x == 0x64)
		fdf->c->off_x -= 10;
	else if (x == 0x61)
		fdf->c->off_x += 10;
	return (0);
}

int	key_hook(int x, t_fdf *fdf)
{
	if (x == 0xff51)
		fdf->c->longitude -= PI / 120;
	else if (x == 0xff53)
		fdf->c->longitude += PI / 120;
	else if (x == 0xff52)
		fdf->c->colatitude -= PI / 120;
	else if (x == 0xff54)
		fdf->c->colatitude += PI / 120;
	else if (x == 0xff1b)
		mlx_loop_end(fdf->d->mlx);
	else if (x == 0xff55)
		fdf->c->hscale = fdf->c->hscale + 0.01;
	else if (x == 0xff56)
		fdf->c->hscale = fdf->c->hscale - 0.01;
	else if (x == 0x77 || x == 0x73 || x == 0x61 || x == 0x64)
		wasd_keys(x, fdf);
	if (fdf->c->colatitude > PI)
		fdf->c->colatitude = PI;
	else if (fdf->c->colatitude < 0)
		fdf->c->colatitude = 0;
	while (fdf->c->longitude >= 2 * PI)
		fdf->c->longitude -= 2 * PI;
	while (fdf->c->longitude < 0)
		fdf->c->longitude += 2 * PI;
	return (le_futur(fdf));
}

int	destroy_hook(t_fdf *fdf)
{
	mlx_loop_end(fdf->d->mlx);
	return (0);
}

int	expose_hook(t_fdf *fdf)
{
	le_futur(fdf);
	return (0);
}

int	scroll_hook(int k, int x, int y, t_fdf *fdf)
{
	(void)x;
	(void)y;
	if (k == 4)
		fdf->c->size = (fdf->c->size * 11) / 10 + 1;
	else if (k == 5)
		fdf->c->size = (fdf->c->size * 9) / 10;
	if (fdf->c->size < 2)
		fdf->c->size = 2;
	if (fdf->c->size > (fdf->d->h + fdf->d->w) / 6)
		fdf->c->size = (fdf->d->h + fdf->d->w) / 6;
	le_futur(fdf);
	return (0);
}
