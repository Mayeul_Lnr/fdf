/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/19 17:11:37 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/11/09 14:43:26 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./fdf.h"

int	ft_atoichk(const char *str, int *n)
{
	unsigned long long int	ret;
	int						sgn;

	sgn = 1;
	if (*str == '-')
		sgn = -1;
	if (*str == '-' || *str == '+')
		str++;
	ret = 0;
	while (*str >= '0' && *str <= '9')
	{
		ret = ret * 10 + *str - '0';
		if (ret > 2147483648 || (ret > 2147483647 && sgn > 0))
			return (-1);
		str++;
	}
	if (*str)
		return (-1);
	*n = sgn * ret;
	return (0);
}
