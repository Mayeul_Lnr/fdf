/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/03 14:37:56 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/12/22 17:14:16 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./fdf.h"

int	escape_hook(int x, t_fdf *fdf)
{
	if (x == 0xff1b)
		mlx_loop_end(fdf->d->mlx);
	return (0);
}

int	main(int ac, char **av)
{
	t_fdf		*fdf;

	if (ac != 2)
		return (-1);
	fdf = fdf_init(WIN_W, WIN_H, "Fdf Du FutuuUUUuuuuur");
	if (!fdf)
		return (-2);
	if (fdf_parsing(av[1], fdf->m) < 0)
		return ((long)fdf_destroy(fdf) - 3);
	camera_init(fdf);
	if (!transform_init(fdf))
	{
		mlx_hook(fdf->d->win, 17, 0L, &destroy_hook, fdf);
		mlx_hook(fdf->d->win, 12, 1L << 15, &expose_hook, fdf);
		mlx_hook(fdf->d->win, 2, 1L << 0, &escape_hook, fdf);
		mlx_loop(fdf->d->mlx);
	}
	fdf_destroy(fdf);
	return (0);
}
