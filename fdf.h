/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/03 14:38:30 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/12/22 22:29:41 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <math.h>

# include "./mlx_linux/mlx.h"
# include "./pcr/pcr.h"

# define GNL_BUFFER_SIZE 256
# define PI 3.14159265359

# define WIN_W 1280
# define WIN_H 720

# define CR_MIN 0x004400cc
# define CR_MAX 0x00ff00ff

typedef struct s_map
{
	int	*map;
	int	*transform_x;
	int	*transform_y;
	int	w;
	int	h;
	int	max;
	int	min;
}	t_map;

typedef struct s_list
{
	void			*data;
	struct s_list	*next;
}	t_list;

typedef struct s_camera
{
	double	colatitude;
	double	longitude;
	double	xcentre;
	double	ycentre;
	int		size;
	float	hscale;
	int		off_x;
	int		off_y;
}	t_camera;

typedef struct s_fdf
{
	t_map		*m;
	t_camera	*c;
	t_disp		*d;
}	t_fdf;

/*
**	Parsing functions (in parsing.c)
*/

int		fdf_parsing(char *file, t_map *map);

/*
**	Init functions for fdf (in fdf_init.c)
*/

t_fdf	*fdf_init(int x, int y, char *s);

t_fdf	*fdf_destroy(t_fdf *fdf);

int		camera_init(t_fdf *fdf);

int		transform_init(t_fdf *fdf);

/*
**	X_events managements functions(in hook.c)
*/

int		key_hook(int x, t_fdf *fdf);

int		destroy_hook(t_fdf *fdf);

int		expose_hook(t_fdf *fdf);

int		scroll_hook(int k, int x, int y, t_fdf *fdf);

/*
**	Cyborg functions from The FutuuUUUuuuure(TM) (in le_futur.c)
*/

int		le_futur(t_fdf *fdf);

/*
**	List management functions (in ft_list.c)
*/

int		list_push_back(t_list **list, void *line);

void	*list_free(t_list **list);

/*
**	ft_split. That is all.
*/

char	**ft_split(char const *s, char c, int *size);

void	*ft_splitfree(char **s);

/*
**	get_next_line. That is all.
*/

int		get_next_line(int fd, char **line);

/*
**	ft_atoichk. That is all.
*/

int		ft_atoichk(const char *str, int *n);

#endif
