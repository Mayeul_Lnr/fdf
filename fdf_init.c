/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_init.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/20 15:19:42 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/12/22 20:36:12 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./fdf.h"

t_fdf	*fdf_init(int x, int y, char *s)
{
	t_fdf	*ret;

	ret = malloc(sizeof(t_fdf));
	if (!ret)
		return (NULL);
	ret->m = malloc(sizeof(t_map));
	ret->m->map = NULL;
	ret->m->transform_x = NULL;
	ret->m->transform_y = NULL;
	ret->c = malloc(sizeof(t_camera));
	ret->d = pcr_init_disp(x, y, s);
	if (!ret->m || !ret->c || !ret->d)
		return (fdf_destroy(ret));
	return (ret);
}

t_fdf	*fdf_destroy(t_fdf *fdf)
{
	if (!fdf)
		return (NULL);
	free(fdf->c);
	if (fdf->m)
	{
		free(fdf->m->map);
		free(fdf->m->transform_x);
		free(fdf->m->transform_y);
	}
	free(fdf->m);
	pcr_destroy_disp(fdf->d);
	free(fdf);
	return (NULL);
}

int	camera_init(t_fdf *fdf)
{
	fdf->c->colatitude = 3 * PI / 4;
	fdf->c->longitude = PI / 4;
	fdf->c->xcentre = (double)fdf->m->w / 2;
	fdf->c->ycentre = (double)fdf->m->h / 2;
	fdf->c->size = (2 * (fdf->d->w + fdf->d->h))
		/ (3 * (fdf->m->w + fdf->m->h));
	fdf->c->hscale = 0.1;
	fdf->c->off_x = 0;
	fdf->c->off_y = 0;
	return (0);
}

int	transform_init(t_fdf *fdf)
{
	fdf->m->transform_x = malloc(sizeof(int) * fdf->m->w * fdf->m->h);
	if (!fdf->m->transform_x)
		return (-1);
	fdf->m->transform_y = malloc(sizeof(int) * fdf->m->w * fdf->m->h);
	if (!fdf->m->transform_y)
		return (-1);
	return (0);
}
