MANDA_SRCS 	=	main.c

BONUS_SRCS	=	main_bonus.c

COMMON_SRCS	=	parsing.c \
				fdf_init.c \
				le_futur.c \
				ft_split.c \
				gnl.c \
				ft_atoi.c \
				ft_list.c \
				hook.c \
				pcr/pcr_line_pixel.c \
				pcr/pcr_line_fade.c \
				pcr/pcr_init.c \
				pcr/pcr_color_op.c

OBJS		= ${COMMON_SRCS:.c=.o} ${MANDA_SRCS:.c=.o}
BONUS_OBJS	= ${COMMON_SRCS:.c=.o} ${BONUS_SRCS:.c=.o}

NAME		= fdf
CC			= clang -Wall -Wextra -Werror
FLAGS		= -Lmlx_linux -lmlx_Linux -L/usr/lib -Imlx_linux -lXext -lX11 -lm -lz
RM			= rm -f

${NAME}:	${OBJS}
	${MAKE} -C mlx_linux
	${CC} ${FLAGS} ${OBJS} mlx_linux/libmlx_Linux.a -o ${NAME}

all:		${NAME}

bonus:		${BONUS_OBJS}
	${CC} ${FLAGS} ${BONUS_OBJS} mlx_linux/libmlx_Linux.a -o ${NAME}

%.o:		%.c
	clang -Werror -Wextra -Wall -I/usr/include -Imlx_linux -c $< -o $@

clean:
	${RM} ${OBJS} ${BONUS_OBJS}

fclean:		clean
	${RM} ${NAME}

re:			fclean all

.PHONY:		all bonus clean fclean re
